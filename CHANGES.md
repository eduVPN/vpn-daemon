# ChangeLog

## 3.2.2 (2025-01-22)
- update Go dependencies
- fix some _linter_ issues

## 3.2.1 (2024-11-12)
- update Go dependencies
- run `gofumpt` source formatter
- fully move to `codeberg.org`

## 3.2.0 (2024-08-14)
- expose number of connected VPN clients if requested
  ([#4](https://codeberg.org/eduVPN/vpn-daemon/issues/4))
- do not return HTTP error responses in case we are unable to obtain list of
  connected VPN clients, instead log error and return empty list
- update deprecated `ioutil.ReadFile` to `os.ReadFile`
- update Go dependencies

## 3.1.3 (2024-08-05)
- update dependencies

## 3.1.2 (2024-07-05)
- fix building on all targets, only support WireGuard on Linux

## 3.1.1 (2024-07-05)
- fix building on OpenBSD

## 3.1.0 (2024-07-04)
- update Go dependencies
- pull the `wgctrl-go` code we use in our tree
- allowing setting node in "maintenance mode" to allow portal/controller to 
  ignore it when assigning clients to nodes

## 3.0.6 (2024-04-18)
- update Go dependencies

## 3.0.5 (2023-08-08)
- return empty list on `/w/peer_list` calls instead of HTTP 500 when there is 
  no WireGuard device

## 3.0.4 (2023-05-12)
- update `go.mod` to require at least Go 1.18

## 3.0.3 (2023-05-10)
- expose `node_uptime` in `/i/node` response indicating the node's uptime in 
  seconds

## 3.0.2 (2022-12-01)
- update Go dependencies
- `/w/remove_peer` returns 204 ("No Content") instead of 404 when peer is not 
  there, just like `/o/disconnect_client`

## 3.0.1 (2022-09-08)
- update Go dependencies
- update documentation
- fix issue found by "staticcheck"

## 3.0.0 (2022-05-09)
- implement support for WireGuard
- switch to HTTP(S) from TCP socket
- use Go modules
- implement "Node Info" endpoint
- use `CREDENTIALS_DIRECTORY` to point to TLS certificates
- use filesystem sockets for OpenVPN management interface

## 1.2.0 (2020-05-13)
- allow specifying full path to `tlsCaPath`, `tlsCertPath` and `tlsKeyPath` 
  isntead of folders allowing for more flexibility and easier packaging

## 1.1.1 (2020-05-01)
- update `Makefile` to support `install`

## 1.1.0 (2020-04-16)
- also return management port when using `LIST` command to be able to link 
  client connections to a particular OpenVPN process

## 1.0.1 (2020-04-08)
- fix parsing of OpenVPN status commando when no IP addresses are set 
  (issue #53)

## 1.0.0 (2019-11-18)
- initial release
