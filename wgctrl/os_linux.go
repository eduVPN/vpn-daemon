//go:build linux

package wgctrl

import (
	"codeberg.org/eduVPN/vpn-daemon/wgctrl/internal/wginternal"
	"codeberg.org/eduVPN/vpn-daemon/wgctrl/internal/wglinux"
)

// newClients configures wginternal.Clients for Linux systems.
func newClients() ([]wginternal.Client, error) {
	var clients []wginternal.Client

	// Linux has an in-kernel WireGuard implementation. Determine if it is
	// available and make use of it if so.
	kc, ok, err := wglinux.New()
	if err != nil {
		return nil, err
	}
	if ok {
		clients = append(clients, kc)
	}

	return clients, nil
}
