//go:build !linux

package wgctrl

import (
	"codeberg.org/eduVPN/vpn-daemon/wgctrl/internal/wginternal"
)

func newClients() ([]wginternal.Client, error) {
	return nil, wginternal.ErrNotImplemented
}
