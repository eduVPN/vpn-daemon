package wginternal

import (
	"errors"
	"io"

	"codeberg.org/eduVPN/vpn-daemon/wgctrl/wgtypes"
)

var ErrNotImplemented = errors.New("not implemented")

// A Client is a type which can control a WireGuard device.
type Client interface {
	io.Closer
	Devices() ([]*wgtypes.Device, error)
	Device(name string) (*wgtypes.Device, error)
	ConfigureDevice(name string, cfg wgtypes.Config) error
}
