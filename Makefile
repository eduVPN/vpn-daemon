PREFIX=/usr/local

.PHONY: update vendor fmt lint check vet clean install sloc

vpn-daemon: cmd/vpn-daemon/main.go
	go build $(GOBUILDFLAGS) -o $@ codeberg.org/eduVPN/vpn-daemon/cmd/vpn-daemon/...

update:
	# update Go dependencies
	go get -t -u codeberg.org/eduVPN/vpn-daemon/cmd/...
	go mod tidy

vendor:
	go mod vendor

fmt:
	gofumpt -w . || go fmt ./...

lint:
	golangci-lint run -E stylecheck,revive,gocritic

check:
	# https://staticcheck.io/
	staticcheck codeberg.org/eduVPN/vpn-daemon/cmd/...

vet:
	go vet codeberg.org/eduVPN/vpn-daemon/cmd/...

sloc:
	tokei cmd/vpn-daemon wgctrl || cloc cmd/vpn-daemon wgctrl

clean:
	rm -f vpn-daemon
	rm -rf vendor

install: vpn-daemon
	install -D vpn-daemon $(DESTDIR)$(PREFIX)/sbin/vpn-daemon
