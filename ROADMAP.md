# Roadmap

## 4.x

We want to be able to configure VPN node(s) without needing to talk to the 
controller(s). A possible way could be that the controller pushes the node's 
configuration to `vpn-daemon`, where `vpn-daemon` stores it in the local state
directory where a "apply changes" script can take it and configure it in e.g.
`/etc/wireguard/wg0.conf`.

Perhaps we can have systemd unit that watches a file and then triggers an 
"apply changes" script, or a cron that takes action after a file change. An 
example for exactly this can be found 
[here](https://web.archive.org/save/https://superuser.com/questions/1171751/restart-systemd-service-automatically-whenever-a-directory-changes-any-file-ins).
